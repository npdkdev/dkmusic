#
# Copyright (C) 2021-2022 by TeamYukki@Github, < https://github.com/TeamYukki >.
#
# This file is part of < https://github.com/NpDkProjects/NpDkMan > project,
# and is released under the "GNU v3.0 License Agreement".
# Please see < https://github.com/NpDkProjects/NpDkMan/blob/master/LICENSE >
#
# All rights reserved.
#

from .config import *
